{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Kaggle Titanic survival - logistic regression model\n",
    "\n",
    "In this notebook we repeat our basic logistic regression model as previously described:\n",
    "\n",
    "https://github.com/MichaelAllen1966/1804_python_healthcare/blob/master/titanic/02_logistic_regression.ipynb\n",
    "\n",
    "We will extend the model to report a range of accuracy measures, as described:\n",
    "\n",
    "https://github.com/MichaelAllen1966/1804_python_healthcare/blob/master/titanic/05_accuracy_standalone.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will go through the following steps:\n",
    "\n",
    "* Download and save pre-processed data\n",
    "* Split data into features (X) and label (y)\n",
    "* Split data into training and test sets (we will test on data that has not been used to fit the model)\n",
    "* Standardise data\n",
    "* Fit a logistic regression model (from sklearn learn)\n",
    "* Predict survival of the test set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load modules\n",
    "\n",
    "A standard Anaconda install of Python (https://www.anaconda.com/distribution/) contains all the necessary modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "# Import machine learning methods\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.preprocessing import StandardScaler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data\n",
    "\n",
    "The section below downloads pre-processed data, and saves it to a subfolder (from where this code is run).\n",
    "If data has already been downloaded that cell may be skipped.\n",
    "\n",
    "Code that was used to pre-process the data ready for machine learning may be found at:\n",
    "https://github.com/MichaelAllen1966/1804_python_healthcare/blob/master/titanic/01_preprocessing.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "download_required = True\n",
    "\n",
    "if download_required:\n",
    "    \n",
    "    # Download processed data:\n",
    "    address = 'https://raw.githubusercontent.com/MichaelAllen1966/' + \\\n",
    "                '1804_python_healthcare/master/titanic/data/processed_data.csv'\n",
    "    \n",
    "    data = pd.read_csv(address)\n",
    "\n",
    "    # Create a data subfolder if one does not already exist\n",
    "    import os\n",
    "    data_directory ='./data/'\n",
    "    if not os.path.exists(data_directory):\n",
    "        os.makedirs(data_directory)\n",
    "\n",
    "    # Save data\n",
    "    data.to_csv(data_directory + 'processed_data.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = pd.read_csv('data/processed_data.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first column is a passenger index number. We will remove this, as this is not part of the original Titanic passenger data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop Passengerid (axis=1 indicates we are removing a column rather than a row)\n",
    "# We drop passenger ID as it is not original data\n",
    "\n",
    "data.drop('PassengerId', inplace=True, axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Divide into X (features) and y (labels)\n",
    "\n",
    "We will separate out our features (the data we use to make a prediction) from our label (what we are truing to predict).\n",
    "By convention our features are called `X` (usually upper case to denote multiple features), and the label (survive or not) `y`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = data.drop('Survived',axis=1) # X = all 'data' except the 'survived' column\n",
    "y = data['Survived'] # y = 'survived' column from 'data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Divide into training and tets sets\n",
    "\n",
    "When we test a machine learning model we should always test it on data that has not been used to train the model.\n",
    "We will use sklearn's `train_test_split` method to randomly split the data: 75% for training, and 25% for testing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standardise data\n",
    "\n",
    "We want all of out features to be on roughly the same scale. This generally leads to a better model, and also allows us to more easily compare the importance of different features. A common method used in many machine learning methods is standardisation, where we use the mean and standard deviation of the training set of data to normalise the data. We subtract the mean of the test set values, and divide by the standard deviation of the training data. Note that the mean and standard deviation of the training data are used to standardise the test set data as well. Here we will use sklearn's `StandardScaler method`. This method also copes with problems we might otherwise have (such as if one feature has zero standard deviation in the training set)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def standardise_data(X_train, X_test):\n",
    "    \n",
    "    # Initialise a new scaling object for normalising input data\n",
    "    sc = StandardScaler() \n",
    "\n",
    "    # Set up the scaler just on the training set\n",
    "    sc.fit(X_train)\n",
    "\n",
    "    # Apply the scaler to the training and test sets\n",
    "    train_std=sc.transform(X_train)\n",
    "    test_std=sc.transform(X_test)\n",
    "    \n",
    "    return train_std, test_std"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train_std, X_test_std = standardise_data(X_train, X_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fit logistic regression model\n",
    "\n",
    "Now we will fir a logistic regression model, using sklearns `LogisticRegression` method. Our machine learning model fitting is only two lines of code!\n",
    "By using the name `model` for ur logistic regression model we will make our model more interchangeable later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,\n",
       "                   intercept_scaling=1, l1_ratio=None, max_iter=100,\n",
       "                   multi_class='warn', n_jobs=None, penalty='l2',\n",
       "                   random_state=None, solver='lbfgs', tol=0.0001, verbose=0,\n",
       "                   warm_start=False)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model = LogisticRegression(solver='lbfgs')\n",
    "model.fit(X_train_std,y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Predict values\n",
    "\n",
    "Now we can use the trained model to predict survival. We will test the accuracy of both the training and test data sets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Predict training and test set labels\n",
    "y_pred_train = model.predict(X_train_std)\n",
    "y_pred_test = model.predict(X_test_std)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
